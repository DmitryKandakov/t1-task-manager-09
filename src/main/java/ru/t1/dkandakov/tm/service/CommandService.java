package ru.t1.dkandakov.tm.service;

import ru.t1.dkandakov.tm.api.ICommandRepository;
import ru.t1.dkandakov.tm.api.ICommandService;
import ru.t1.dkandakov.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}